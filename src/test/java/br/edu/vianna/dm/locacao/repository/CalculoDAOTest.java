/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.repository;

import br.edu.vianna.dm.locacao.model.Calculo;
import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author daves h2
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class CalculoDAOTest {
    
    @Autowired
    CalculoDAO cDao;
    
    @BeforeEach
    public void init(){
        Calculo c1 = new Calculo(1, "Exemplo01", LocalDateTime.now(), 5.0, 8.0);
        cDao.save( c1 );
//        System.out.println("==>"+c1.getId());
//        System.out.println("==>"+cDao.count());
    }
    
    @AfterEach
    public void finish(){
        cDao.deleteAll();
    }

    @Test
    public void alteracao() {
        //cenário  
        Calculo c = cDao.getOne((long)1); 
        c.setNome("Zezin");
        cDao.save(c);
                
        //execução   
        Calculo c1 = cDao.getOne((long)1); 
        
        //verificação
       // Assertions.assertThat(c.soma()).isEqualTo(13);
        Assertions.assertThat(c1.getNome()).isEqualTo(c.getNome());        
    }

    @Test
    public void testSomeMethod() {
        //cenário                
        
        //execução
        Calculo c = cDao.findByNome("Exemplo01");       
        
        //verificação
       // Assertions.assertThat(c.soma()).isEqualTo(13);
        Assertions.assertThat(c.getValor1()).isEqualTo(5.0);        
    }
    
}
