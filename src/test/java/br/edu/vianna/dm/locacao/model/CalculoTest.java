/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author daves
 */
public class CalculoTest {

    public CalculoTest() {
    }

    @Test
    //Soma 2 numeros positivos
    public void testasomaNumerosPositivos() {
        //Cenários
        Calculo c = new Calculo(5, 8);

        //Execução        
        double resultado = c.soma();

        //Verificação //Junit //AsserJ
        assertEquals(13, resultado);
        Assertions.assertThat(resultado).isEqualTo(13);
    }

    @Test
    //Soma 1 numeros positivos e um 0
    public void testasoma1NumeroPositivo1Zero() {
        //Cenários
        Calculo c = new Calculo(5, 0);

        //Execução        
        double resultado = c.soma();

        //Verificação //Junit //AsserJ
        //assertEquals(5, resultado);
        Assertions.assertThat(resultado).isEqualTo(5);
    }

    @Test
    //Soma 2 numeros negtivos
    public void testasoma1NumeroNegativos() {
        //Cenários
        Calculo c = new Calculo(-3, -5);

        //Execução        
        double resultado = c.soma();

        //Verificação //Junit //AsserJ
        Assertions.assertThat(resultado).isEqualTo(-8);
    }

    @Test
    //Soma 2 numeros positivos muito Grande
    public void testasoma1NumeroPositivosGrandes() {
        //Cenários
        System.out.println("-->" + Double.MAX_VALUE);
        Calculo c = new Calculo(Double.MAX_VALUE, Double.MAX_VALUE);

        //Execução        
        double resultado = c.multiplicacao();

        //Verificação //Junit //AsserJ
        Assertions.assertThat(resultado).isEqualTo(Double.MAX_VALUE * Double.MAX_VALUE);
    }

    @Test
    //divisao por zeo
    public void testaDivisaoPorZero() {
        //Cenários
        Calculo c = new Calculo(10, 0);

        //Execução        
        try {
            double resultado = c.divisao();
            fail("Não deveria executar divisão por zero");
        } catch (Exception e) {   
            //assertTrue(true);
            Assertions.assertThatExceptionOfType(Exception.class);
        }
    }

}
