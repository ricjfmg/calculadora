/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.service;

import br.edu.vianna.dm.locacao.model.Calculo;
import br.edu.vianna.dm.locacao.repository.CalculoDAO;
import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author daves
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class CalculoServiceTest {
    
    @InjectMocks
    CalculoService calculo;
    @MockBean
    private CalculoDAO calcDao;
    
     @BeforeEach
    public void init(){
       MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSomeMethod() {
        //cenário
        Calculo c = new Calculo(1, "abc", LocalDateTime.now(), 5.0, 8.0);
        Mockito.when(calcDao.save(c)).thenReturn(null);
        //execução
        Calculo c1 = calculo.salvarOperacao(5, 8);
        
        //verificação
        Assertions.assertThat(c1.getNome()).isEqualTo(c.getNome());        
    }
    
}
