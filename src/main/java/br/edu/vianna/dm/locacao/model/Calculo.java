package br.edu.vianna.dm.locacao.model;

import br.edu.vianna.dm.locacao.exceptions.DivisaoPorZeroException;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Calculo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nome;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    @Column(columnDefinition = "DATE")
    private LocalDateTime dataLocacao;

    private Double valor1, valor2;

    public Calculo(double v1, double v2) {
        this.valor1 = v1;
        this.valor2 = v2;
    }

    public double soma(){        
        
        return valor1 + valor2;
        
    }

    public double subtracao(){        
        
        return valor1 - valor2;
        
    }

    public double divisao() throws DivisaoPorZeroException{        
        if (valor2 == 0){
            throw new DivisaoPorZeroException();
        }        
        return valor1/ valor2;
        
    }

    public double multiplicacao(){        
        
        return valor1 * valor2;
        
    }
    
    
    public Calculo() {
    }

    public Calculo(long id, String nome, LocalDateTime dataLocacao, Double valor1, Double valor2) {
        this.id = id;
        this.nome = nome;
        this.dataLocacao = dataLocacao;
        this.valor1 = valor1;
        this.valor2 = valor2;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDateTime getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(LocalDateTime dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public Double getValor1() {
        return valor1;
    }

    public void setValor1(Double valor1) {
        this.valor1 = valor1;
    }

    public Double getValor2() {
        return valor2;
    }

    public void setValor2(Double valor2) {
        this.valor2 = valor2;
    }
    
    
    

}
