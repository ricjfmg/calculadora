/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.dm.locacao.repository;

import br.edu.vianna.dm.locacao.model.Calculo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daves
 */

public interface CalculoDAO extends JpaRepository<Calculo, Long>{
    
    
    public Calculo findByNome(String nome);
    
    
}
