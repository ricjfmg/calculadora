package br.edu.vianna.dm.locacao.service;

import br.edu.vianna.dm.locacao.model.Calculo;
import br.edu.vianna.dm.locacao.repository.CalculoDAO;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculoService {

    @Autowired
    CalculoDAO cDao;
    
    public Calculo salvarOperacao(double valor1, double valor2){        
        Calculo c = new Calculo(valor1,valor2);
        
        c.setDataLocacao(LocalDateTime.now());
        c.setNome("abc");
        
        cDao.save(c);       
        
        return c;        
    }
    

}
